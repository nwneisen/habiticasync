// Your habitica API information
var habId = "acd213b6-69cf-458a-a87e-dc853e1698ff";
var habToken = "daaf8d9d-27be-4ab1-a022-0b37cb51566b";

// Boilerplate for a habitica API call
var postTemplate = {
  "method" : "post",
  "headers" : {
    "x-api-user" : habId, 
    "x-api-key" : habToken
  }
}

var getTemplate = {
  "method" : "get",
  "headers" : {
    "x-api-user" : habId, 
    "x-api-key" : habToken
  }
}

var gmailHabits = {};
var gmailTodos = {};
var gmailTodosId; 
var gmailDailies = {};

var habiticaTasks = {};
var habiticaTodos = {};
var habiticaDailies = {};
var habiticaHabits = {};

var now = new Date();

// Main entry point
function sync() {
  getHabiticaTasks();
  
  // Setup our Habitica dictionaries
  generateHabiticaDailies();
  generateHabiticaTodos();
  generateHabiticaHabits();
  
  // Setup our Gmail dictionaries
  generateGmailDailies();
  generateGmailTodos();
  generateGmailHabits();
  
  // Sync the lists
  if(gmailDailies && habiticaDailies)
    syncDailies();
  
  if(gmailTodos && habiticaTodos)
    syncToDos();
  
  if(gmailHabits && habiticaHabits)
    syncHabits();
}

// Loops each dictionary and makes an api call for missing tasks
function syncDailies() { 
  for(var key in gmailDailies)
    addHabiticaDaily(gmailDailies[key]);
  
  for(var key in habiticaDailies)
    addGmailDaily(habiticaDailies[key]);
}

// Loops each dictionary and makes an api call for missing tasks
function syncToDos() { 
  for(var key in gmailTodos)
    addHabiticaTodo(gmailTodos[key]);
  
  for(var key in habiticaTodos)
    addGmailTodo(habiticaTodos[key]);
}

// Loops each dictionary and makes an api call for missing tasks
function syncHabits() { 
  for(var key in gmailHabits)
    addHabiticaHabit(gmailHabits[key]);
  
  for(var key in habiticaHabits)
    addGmailHabit(habiticaHabits[key]);
}

// Makes a request to the Habitica api and creates a dictionary of results
function generateHabiticaTodos() {  
  if(!habiticaTasks) {
    Logger.log("Habitica tasks do not exist");
    return;
  }  
  
  for(var i=0; i<habiticaTasks.data.length; ++i) {
    var task = habiticaTasks.data[i];
    if(task.type == "todo") {    
      habiticaTodos[task.text] = {
        title : task.text,
        notes : task.notes,
        tags : task.tags
      }
    }
  }
  
  if(Object.keys(habiticaTodos).length == 0) {
    Logger.log("Todo list was empty in Habitica");
  }
}

// Sends a new todo through the habitica api
function addHabiticaTodo(task) {
  if(!habiticaTodos) {
    Logger.log("Habitica To-Dos do not exist");
    return;
  }
  
  if(!habiticaTodos[task.title]) {     
    var params = postTemplate;
    var payload = {
      "text" : task.title, 
      "type" : "todo",
      "priority" : "1.5",
      "notes" : task.notes,
      "date" : task.due
    };
    params["payload"] = payload;    
    UrlFetchApp.fetch("https://habitica.com/api/v3/tasks/user", params)
    
    Logger.log("Add Habitica Todo: " + task.title);
  }
}

// Gets all of the Habitica tasks for user
function getHabiticaTasks() {
  var params = getTemplate;
  var response = UrlFetchApp.fetch("https://habitica.com/api/v3/tasks/user", params)
  var json = response.getContentText();
  habiticaTasks = JSON.parse(json);
}

// Pulls the Daily calendar and creates a dicitonary of results
function generateGmailDailies() {
  var dailiesCalendar = CalendarApp.getCalendarsByName("Dailies")[0];
  if(!dailiesCalendar) {
    Logger.log("Dailies calendar does not exist in Gmail");
  }
  
  var dailiesEvents = dailiesCalendar.getEventsForDay(now);
  for(i = 0; i < dailiesEvents.length; i++) {
    gmailDailies[dailiesEvents[i].getTitle()] = {
      title : dailiesEvents[i].getTitle()
    }
  }
  
  if(Object.keys(gmailDailies).length == 0) {
    Logger.log("Dailies calendar was empty in Gmail");
    return;
  }
}

// Adds a new task to the Daily calendar
function addGmailDaily(task) {
  if(!gmailDailies) {
    Logger.log("Gmail Dailies do not exist");
    return;
  }
  
  if(!gmailDailies[task.title]) {
    var dailiesCalendar = CalendarApp.getCalendarsByName("Dailies")[0];
    dailiesCalendar.createAllDayEvent(task.title, now);    
    Logger.log("Add calendarDaily: " + task.title);
  }
}

// Creates a dictionary of Habitica daily tasks
function generateHabiticaDailies() {
  getHabiticaTasks();
  
  if(!habiticaTasks) {
    Logger.log("Habitica tasks do not exist");
    return;
  }  
  
  for(var i=0; i<habiticaTasks.data.length; ++i) {
    var task = habiticaTasks.data[i];
    if(task.type == "daily") {    
      habiticaDailies[task.text] = {
        title : task.text,
        notes : task.notes
      }
    }
  }
  
  if(Object.keys(habiticaDailies).length == 0) {
    Logger.log("Dailies list was empty in Habitica");
  }
}

// Sends a new daily to Habitica
function addHabiticaDaily(task) {
  if(!habiticaDailies) {
    Logger.log("Habitica Dailies do not exist");
    return;
  }
  
  if(!habiticaDailies[task.title]) {
    var params = postTemplate;
    var payload = {
      "text" : task.title, 
      "type" : "todo",
      "priority" : "1.5",
      "notes" : task.notes,
      "date" : task.due
    };
    params["payload"] = payload;    
    UrlFetchApp.fetch("https://habitica.com/api/v3/tasks/user", params)
    
    Logger.log("Add Habitica Daily: " + task.title);
  }
}

// Creates a dictionary of Habitica habits
function generateHabiticaHabits() {
  getHabiticaTasks();
  
  if(!habiticaTasks) {
    Logger.log("Habitica tasks do not exist");
    return;
  }  
  
  for(var i=0; i<habiticaTasks.data.length; ++i) {
    var task = habiticaTasks.data[i];
    if(task.type == "habit") {    
      habiticaHabits[task.text] = {
        title : task.text,
        notes : task.notes
      }
    }
  }
  
  if(Object.keys(habiticaHabits).length == 0) {
    Logger.log("Habits list was empty in Habitica");
  }
}

// Sends a new habit to Habitica
function addHabiticaHabit(task) {
  if(!habiticaHabits) {
    Logger.log("Habitica Habits do not exist");
    return;
  }
  
  if(!habiticaHabits[task.title]) {
    var newTask = {
      title: task.text,
      notes: task.notes
    };
    var params = postTemplate;
    var payload = {
      "text" : task.title, 
      "type" : "todo",
      "priority" : "1.5",
      "notes" : task.notes,
      "date" : task.due
    };
    params["payload"] = payload;    
    UrlFetchApp.fetch("https://habitica.com/api/v3/tasks/user", params)
    
    Logger.log("Add Habitica Habit: " + task.title);
  }
}

// Generates a dictionary from the gmail habits calendar
function generateGmailHabits() {
  var habitsCalendar = CalendarApp.getCalendarsByName("Habits")[0];
  if(!habitsCalendar) {
    Logger.log("Habits calendar does not exist in Gmail");
    return;
  }
  
  var habitsEvents = habitsCalendar.getEventsForDay(now);
  for(i = 0; i < habitsEvents.length; i++) {
    gmailHabits[habitsEvents[i].getTitle()] = {
      title : habitsEvents[i].getTitle()
    }
  }
  
  if(Object.keys(gmailHabits).length == 0) {
    Logger.log("Habits calendar was empty in Gmail");
    return;
  }
}

// Adds a new habit to the gmail calendar
function addGmailHabit(task) {
  if(!gmailHabits) {
    Logger.log("Gmail Habits do not exist");
    return;
  }
  
  if(!gmailHabits[task.title]) {
    var habitsCalendar = CalendarApp.getCalendarsByName("Habits")[0];
    habitsCalendar.createAllDayEvent(task.title, now);    
    Logger.log("Add calendarHabit: " + task.title);
  }
}

// Creates a dictionary of tasks from gmail todo lists
function generateGmailTodos() {
  var taskLists = Tasks.Tasklists.list(); 
  if(!taskLists.items) {
    Logger.log("No tasks lists in Gmail");
    return;
  }
  
  for (var i = 0; i < taskLists.items.length; i++) {  
    var taskList = taskLists.items[i];
    if(taskList.title == "To-Dos") {
      gmailTodosId = taskList.id;
    }
    
    var tasks = Tasks.Tasks.list(taskList.id).items;  
    if(tasks) {
      for (var j = 0; j < tasks.length; j++) {     
        gmailTodos[tasks[j].title] = {
          title : tasks[j].title,
          notes : tasks[j].notes,
          tags : [ taskList.title ]
        }
      }
    }
  }
  
  if(!gmailTodosId) {
    Logger.log("To-Dos task list not present in Gmail");
  }
  
  if(Object.keys(gmailTodos).length == 0) {
    Logger.log("Tasks lists were empty in Gmail");
  }
}

// Adds a new task to the gmail todo task list
function addGmailTodo(task) {
  if(!gmailTodosId || !gmailTodos) {
    Logger.log("Gmail To-Dos do not exist");
    return;
  }
  
  if(!gmailTodos[task.title]) {
    var newTask = {
      title: task.title,
      notes: task.notes
    };
    Tasks.Tasks.insert(newTask, gmailTodosId);    
    Logger.log("Add calendarTask: " + task.title);
  }
}